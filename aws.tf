resource "aws_instance" "demo-vm" {
  ami = "ami-0c02fb55956c7d316"
  instance_type = "t2.micro"
  tags = {
    Name = "demo-vm"
  }
  availability_zone = "us-east-1b"
}