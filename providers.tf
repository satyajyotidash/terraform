provider "aws" {
  region = "us-east-1"
}
terraform {
  backend "s3" {
    bucket = "indsatyabucket-250693"
    key            = "terraform.tfstate"
    region         = "us-east-1"
    dynamodb_table = "terraformlocktable2"
  }
}